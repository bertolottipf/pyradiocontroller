# __PyRadioController__ #

Questo programma serve per poter "manovrare" a distanza la propria pyradio 


### Come si utilizza? ###
    :::sh
    sudo ./pyradio_server.py
    sudo ./pyradio_client.py


### Come creare l'apparato hardware? ###

Per creare l'apparato hardware mi sono basato esclusivamente sui seguenti siti.

* http://www.gmpa.it/it9xxs/?cat=10
* http://raspberry-python.blogspot.it/2013/02/making-your-own-raspberrypi-gpio-cable.html

Per la parte di configurazione ho configurato il wi-fi ho editato il file /etc/wpa_supplicant/wpa_supplicant.conf

    :::sh
    sudo nano /etc/wpa_supplicant/wpa_supplicant.conf

incollando al suo interno
    
    :::text
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1

    network={
            ssid="il_nome_della_mia_rete"
            scan_ssid=1
            psk="la_password_d_accesso_alla_rete"
    }

per fare partire in automatico il server ho creato il file */etc/radio_server* dando i permessi 755
    
    :::sh
    touch /etc/init.d/radio_server
    sudo chmod 755 /etc/init.d/radio_server
    sudo nano /etc/init.d/radio_server

incollando al suo interno

    :::text
    ### BEGIN INIT INFO
    # Provides: radio_server 
    # Required-Start: $remote_fs $syslog
    # Required-Stop: $remote_fs $syslog
    # Default-Start: 2 3 4 5
    # Default-Stop: 0 1 6
    # Short-Description: server remote controll RPI WebRadio 
    # Description: server for remote controll for the RPI Web Radio Receiver
    ### END INIT INFO

    #! /bin/sh
    # /etc/init.d/radio_server

    export HOME
    case "$1" in
        start)
            echo "Starting RPI_WebRadio_Server"
            /home/pi/radio_server.py  2>&1 &
        ;;
        stop)
            echo "Stopping RPI_WebRadio_Server"
        LCD_PID=`ps auxwww | grep radio_server | head -1 | awk '{print $2}'`
        kill -9 $LCD_PID
        ;;
        *)
            echo "Usage: /etc/init.d/radio_server {start|stop}"
            exit 1
        ;;
    esac
    exit 0
    

ovviamente copiando il file pyradio_server.py nella home dell'user pi