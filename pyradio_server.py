#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

from socket import *
import os
import subprocess


server_socket = socket(AF_INET, SOCK_STREAM)

class Server:
    
    IP = ""
    PORT = 8000
    BUFSIZE = 4096

    def getNumRadio(self):
        cmd = subprocess.Popen("sudo mpc -q playlist",shell=True, stdout=subprocess.PIPE)
        stations = cmd.stdout.readlines()
        channelCount = len(stations)
        return channelCount

    #server_socket = socket(AF_INET, SOCK_STREAM)

    def __init__(self):
        server_socket.bind((self.IP, self.PORT))
        server_socket.listen(5)
        while 1:

            
            client_socket, address = server_socket.accept()
            print("Client Connection: " + str(address))

            while 1:
                #Riceviamo i dati inviati dal client.
                data = client_socket.recv(self.BUFSIZE)
                print("Ricevuto: " + data)

                #Controlliamo che il client non abbia inviato un comando di chiusura.
                if data == 'q' or data == 'Q':
                    client_socket.close()
                    break;
                #Se non e' un comando di chiusura facciamo qualcosa con i dati ricevuti.
                else:
                    #FARE QUALCOSA CON I DATI.
                    #Inviare i dati processati dal server.
                    if data == "getNumRadio":
                        client_socket.send(bytes(self.getNumRadio()))
                    else:
                        os.system(data)
                        client_socket.send(bytes("OK"))

            print("Client Disconnection: " + str(address))

server = Server()