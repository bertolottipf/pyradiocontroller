#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

import os
import subprocess
from socket import *
import curses
import sys


client_socket = socket(AF_INET, SOCK_STREAM)

class Client:

    IP = 'raspberrypi'
    PORT = 8000
    volume = 88
    canale = 1
    BUFSIZE = 4096
    data = ''


    stdscr = curses.initscr()
    curses.cbreak()
    stdscr.keypad(1)


    def inits(self):
        self.stdscr.clear()
        self.stdscr.addstr(0,10,"PyRadio remote control")
        self.stdscr.addstr(3,5,"VOLUME: frecce su e giu")
        self.stdscr.addstr(4,5,"VOLUME: frecce destra e sinistra o tasti 1-9")
        self.stdscr.addstr(5,5,"Hit 'q' to quit")
        self.stdscr.refresh()

    def logo(self):
        self.stdscr.clear()
        self.stdscr.addstr(0,14, "¶¶¶¶¶")
        self.stdscr.addstr(1, 2, "¶¶¶¶       ¶¶   ¶¶       ¶¶¶¶")
        self.stdscr.addstr(2, 1, "¶¶  ¶¶        ¶¶¶        ¶¶  ¶¶")
        self.stdscr.addstr(3, 2, "¶¶¶¶¶       ¶¶ ¶¶      ¶¶¶¶¶¶")
        self.stdscr.addstr(4, 6, "¶¶¶¶¶¶¶¶¶¶¶  ¶¶¶¶¶¶¶¶¶¶")
        self.stdscr.addstr(5, 6, "¶¶                 ¶¶")
        self.stdscr.addstr(6, 7, "¶¶               ¶¶")
        self.stdscr.addstr(7, 8, "¶               ¶")
        self.stdscr.addstr(8, 9, "¶             ¶")
        self.stdscr.addstr(9, 9, "¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶")
        self.stdscr.refresh()

    

    def __init__(self):

        self.inits()

        #client_socket = socket(AF_INET, SOCK_STREAM)
        client_socket.connect((self.IP, self.PORT))


        client_socket.send(b"mpc stop")
        resp = client_socket.recv(self.BUFSIZE)
        client_socket.send(b"mpc clear")
        resp1 = client_socket.recv(self.BUFSIZE)
        client_socket.send(b"mpc load mymy")
        resp2 = client_socket.recv(self.BUFSIZE)
        client_socket.send(b"mpc play 1")
        resp3 = client_socket.recv(self.BUFSIZE)
        client_socket.send(b"getNumRadio")
        channelCount = int(client_socket.recv(self.BUFSIZE))

        while 1:
            data = self.stdscr.getch()
            if (data <> ord('Q') and data <> ord('q')):
                if data == curses.KEY_UP:
                    if self.volume < 100:
                        self.volume += 1
                        data = "mpc volume " + str(self.volume)
                    else:
                        data = "mpc volume 100"
                elif data == curses.KEY_DOWN: 
                    if self.volume > 0:
                        self.volume -= 1
                        data = "mpc volume " + str(self.volume)
                    else:
                        data = "mpc volume 0"
                elif data == curses.KEY_RIGHT:
                    if self.canale < channelCount:
                        self.canale += 1
                        data = "mpc play " + str(self.canale)
                    else:
                        data = "mpc play " + str(self.canale)
                elif data == curses.KEY_LEFT: 
                    if self.canale > 1:
                        self.canale -= 1
                        data = "mpc play " + str(self.canale)
                    else:
                        data = "mpc play " + str(self.canale)
                else:
                    data = "mpc"
                
                client_socket.send(bytes(data))
                data = client_socket.recv(self.BUFSIZE)
                print("Ricevuto: ", data)
                
                self.inits()

            else:
                data = "q"
                client_socket.send(bytes(data))
                data = client_socket.recv(self.BUFSIZE)
                client_socket.close()
                break;


        self.logo()





if __name__ == "__main__":
   try:
      client = Client()
   except KeyboardInterrupt:
        data = "q"
        client_socket.send(data)
        client_socket.close()
        sys.exit()